## Non-visual Classes
---
1. ISafeRef<T>: 
    Refernce to an object with the ability to check the object alive
2. TDLinkedList<T>:
    Generic Doubly Circular Linked List
3. TSmartTimer:
    A timer that uses as few system resources.
    As far as possible, this is similar to the behavior of the system timer.
    
## Demos

---

You'll find demo applications in the Demos folder.

## License

---

MIT. See LICENSE file.