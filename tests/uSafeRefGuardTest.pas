unit uSafeRefGuardTest;

interface

uses
  DUnitX.TestFramework;

type

  [TestFixture]
  TSafeRefGuardTest = class(TObject)
  public
    [Test]
    procedure Check_IsAlive;
    [Test]
    procedure Check_Thread_safe_behavior;
  end;

implementation

uses
  Classes, SyncObjs, Windows, SafeRef, SafeRefGuard;

type
  TSafeObj = class
  private
    G: SafeRefG<TSafeObj>;
  public
    constructor Create;
    function GetSafeRef: ISafeRef<TSafeObj>;
  end;

  TTestThread = class(TThread)
  private
    FObj: TSafeObj;
  protected
    procedure Execute; override;
  public
    Events: array [0..1] of TEvent;
    constructor Create(AObj: TSafeObj);
    destructor Destroy; override;
  end;

{ TSafeRefGuardTest }

procedure TSafeRefGuardTest.Check_IsAlive;
var
  AObj: TSafeObj;
  ARef: ISafeRef<TSafeObj>;
begin
  AObj := TSafeObj.Create;
  ARef := AObj.GetSafeRef;

  Assert.AreEqual(true, ARef.IsAlive);

  AObj.Free;
  Assert.AreEqual(false, ARef.IsAlive);
end;

procedure TSafeRefGuardTest.Check_Thread_safe_behavior;
var
  AObj: TSafeObj;
  ARef: ISafeRef<TSafeObj>;

  ATest: TTestThread;
begin
  AObj := TSafeObj.Create;
  ARef := AObj.GetSafeRef;
  ATest := TTestThread.Create(AObj);

  ATest.Events[0].WaitFor;//wait for thread start
  ARef.Execute(
    procedure
    begin
      Assert.AreEqual(true, ARef.IsAlive);
      ATest.Events[1].SetEvent;
      sleep(100);//Thread must in Obj.Free run
      Assert.AreEqual(true, ARef.IsAlive);
    end);

  ATest.WaitFor;
  ATest.Free;
  Assert.AreEqual(false, ARef.IsAlive);
end;

{ TSafeObj }

constructor TSafeObj.Create;
begin
  G.C(TSafeRef<TSafeObj>.Create(Self) as ISafeRef<TSafeObj>);
end;

function TSafeObj.GetSafeRef: ISafeRef<TSafeObj>;
begin
  Result := G;
end;

{ TTestThread }

constructor TTestThread.Create(AObj: TSafeObj);
begin
  FObj := AObj;
  Events[0] := TEvent.Create(nil, true, false, '');
  Events[1] := TEvent.Create(nil, true, false, '');
  inherited Create(false);
end;

destructor TTestThread.Destroy;
var
  i: integer;
begin
  for i := 0 to High(Events) do
      Events[i].Free;
  inherited;
end;

procedure TTestThread.Execute;
begin
  Events[0].SetEvent;
  Events[1].WaitFor;//wait for the start function "Execute"
  FObj.Free;//must wait for "Execute" to finished
end;

initialization

TDUnitX.RegisterTestFixture(TSafeRefGuardTest);

end.
