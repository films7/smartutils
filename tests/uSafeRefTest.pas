unit uSafeRefTest;

interface

uses
  DUnitX.TestFramework;

type

  [TestFixture]
  TSafeRefTest = class(TObject)
  public
    [Test]
    procedure Check_IsAlive;
  end;

implementation

uses
  SafeRef;

type
  TSafeObj = class
  private
    FRef: ISafeRef<TSafeObj>;
  public
    constructor Create;
    destructor Destroy; override;
    function GetSafeRef: ISafeRef<TSafeObj>;
  end;

procedure TSafeRefTest.Check_IsAlive;
var
  AObj: TSafeObj;
  ARef: ISafeRef<TSafeObj>;
begin
  AObj := TSafeObj.Create;
  ARef := AObj.GetSafeRef;

  Assert.AreEqual(true, ARef.IsAlive);

  AObj.Free;
  Assert.AreEqual(false, ARef.IsAlive);
end;

{ TSafeObj }

constructor TSafeObj.Create;
begin
  FRef := TSafeRef<TSafeObj>.Create(Self);
end;

destructor TSafeObj.Destroy;
begin
  FRef.Cleanup;
  inherited;
end;

function TSafeObj.GetSafeRef: ISafeRef<TSafeObj>;
begin
  Result := FRef;
end;

initialization

TDUnitX.RegisterTestFixture(TSafeRefTest);

end.
