unit uSmartTimerTest;

interface

uses
  DUnitX.TestFramework;

type

  [TestFixture]
  TIntervalManagerTest = class(TObject)
  public
    [Test]
    procedure Check_Calc;
    [Test]
    procedure Check_Calc_WithOneTimeTimer;
  end;

implementation

uses SmartTimer;

type
  TTestTimer = class(TSmartTimer)
    FID: integer;
  end;

procedure TIntervalManagerTest.Check_Calc;
var
  AMng: TTimerManager.TIntervalManager;
const
  _Interval = 2;
begin
  TTimerManager.Initialize;
  try
    with TTestTimer.Create(_Interval) do
    begin
      FID := 1;
      Enabled := true;
    end;
    TTimerManager.Default.FIntervalDict.TryGetValue(_Interval, AMng);
    Assert.IsNotNull(AMng);
    AMng.Calc(1);
    with TTestTimer.Create(_Interval) do
    begin
      FID := 2;
      Enabled := true;
    end;
    with TTestTimer.Create(_Interval) do
    begin
      FID := 3;
      Enabled := true;
    end;
    with TTestTimer.Create(_Interval) do
    begin
      FID := 4;
      Enabled := true;
    end;
    AMng.Calc(1); //Fire ID1 and move it to the end of the list

    Assert.AreEqual(1, TTestTimer(AMng.Last.Value).FID);
    AMng.Calc(1); //Fire ID2,ID3,ID4 and move it to the end of the list
    Assert.AreEqual(4, TTestTimer(AMng.Last.Value).FID);

  finally
    TTimerManager.Default.Free;
  end;
end;

procedure TIntervalManagerTest.Check_Calc_WithOneTimeTimer;
var
  AMng: TTimerManager.TIntervalManager;
const
  _Interval = 2;
begin
  TTimerManager.Default;
  try
    with TTestTimer.Create(_Interval, true) do
    begin
      FID := 1;
      Enabled := true;
    end;
    TTimerManager.Default.FIntervalDict.TryGetValue(_Interval, AMng);
    Assert.IsNotNull(AMng);
    AMng.Calc(1);
    with TTestTimer.Create(_Interval, true) do
    begin
      FID := 2;
      Enabled := true;
    end;
    with TTestTimer.Create(_Interval, true) do
    begin
      FID := 3;
      Enabled := true;
    end;
    with TTestTimer.Create(_Interval) do
    begin
      FID := 4;
      Enabled := true;
    end;
    AMng.Calc(1); //Fire ID1 and remove from the list

    Assert.AreEqual(3, AMng.Count);
    AMng.Calc(1); //Fire ID2,ID3,ID4 and remove ID2, ID3
    Assert.AreEqual(1, AMng.Count);
  finally
    TTimerManager.Default.Free;
  end;
end;

initialization

TDUnitX.RegisterTestFixture(TIntervalManagerTest);

end.
