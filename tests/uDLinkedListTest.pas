unit uDLinkedListTest;

interface

uses
  DUnitX.TestFramework, Generics.DLinkedList;

type
  TTestObj = class
  public
    class var created: boolean;
  public
    ID: integer;
    constructor Create(AID: integer);
    destructor Destroy; override;
  end;

  [TestFixture]
  TDLinkedListTest = class(TObject)
  private
    FList: TDLinkedList<TTestObj>;
  public
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    [Test]
    procedure Node_OwnsObjectsTrue_ReturnNil;
    [Test]
    procedure Node_OwnsObjectsFalse_ReturnObj;

    [Test]
    procedure Add_OneNode_CountReturn1_Free_CountReturn0;
    [Test]
    procedure Add_OneNode_Remove_CountReturn0;

    [Test]
    [TestCase('Add10_Remove9', '10,9')]
    [TestCase('Add10_Remove10', '10,10')]
    procedure Add_Remove(const ACountAdd, ACountDel: integer);

    [Test]
    [TestCase('Add10_ReturnTrue', '10, 7, 1')]
    [TestCase('Add10_ReturnFalse', '10, 17, 0')]
    [TestCase('Add0_ReturnFalse', '0, 7, 0')]
    [TestCase('Add1_ReturnTrue', '1, 0, 1')]
    procedure FindFirst(const ACountAdd, AID, Res: integer);

    [Test]
    [TestCase('Add1_Del1', '1')]
    [TestCase('Add2_Del2', '2')]
    [TestCase('Add0_Del0', '0')]
    [TestCase('Add3_Del3', '3')]
    procedure FindFirst_WithRemove(const ACount: integer);

    [Test]
    [TestCase('Add10_ReturnTrue', '10, 7, 1')]
    // [TestCase('Add10_ReturnFalse', '10, 17')]
    [TestCase('Add0_ReturnFalse', '0, 7, 0')]
    [TestCase('Add1_ReturnTrue', '1, 0, 1')]
    procedure FindLast(const ACountAdd, AID, Res: integer);

    [Test]
    [TestCase('Add1_Del1', '1')]
    [TestCase('Add2_Del2', '2')]
    [TestCase('Add0_Del0', '0')]
    [TestCase('Add3_Del3', '3')]
    procedure FindLast_WithRemove(const ACount: integer);

    [Test]
    [TestCase('Add10', '10')]
    [TestCase('Add0', '0')]
    [TestCase('Add1', '1')]
    procedure ForEach(const ACount: integer);

    [Test]
    [TestCase('Add1_Del1', '1')]
    [TestCase('Add2_Del2', '2')]
    [TestCase('Add0_Del0', '0')]
    [TestCase('Add3_Del3', '3')]
    procedure ForEach_WithRemove(const ACount: integer);
  end;

implementation

uses SysUtils;

procedure TDLinkedListTest.Setup;
begin
  FList := TDLinkedList<TTestObj>.Create;
end;

procedure TDLinkedListTest.Add_OneNode_CountReturn1_Free_CountReturn0;
var
  AList: TDLinkedList<TTestObj>;
begin
  AList := TDLinkedList<TTestObj>.Create;
  AList.Add(TTestObj.Create(0));
  AList.First.OwnsObjects := true;

  Assert.AreEqual(true, TTestObj.created);
  Assert.AreEqual(1, AList.Count);
  AList.Free;
  Assert.AreEqual(false, TTestObj.created);
end;

procedure TDLinkedListTest.Add_OneNode_Remove_CountReturn0;
var
  AList: TDLinkedList<TTestObj>;
begin
  AList := TDLinkedList<TTestObj>.Create;
  AList.Add(TTestObj.Create(0));
  AList.First.OwnsObjects := true;

  Assert.AreEqual(AList.Last, AList.First);

  Assert.AreEqual(true, TTestObj.created);
  Assert.AreEqual(1, AList.Count);

  AList.Remove(AList.First);
  Assert.AreEqual(false, TTestObj.created);
  Assert.AreEqual(0, AList.Count);

  AList.Free;
  Assert.AreEqual(false, TTestObj.created);
end;

procedure TDLinkedListTest.Add_Remove(const ACountAdd, ACountDel: integer);
var
  i: integer;
begin
  for i := 0 to ACountAdd - 1 do
  begin
    FList.Add(TTestObj.Create(i));
    FList.Last.OwnsObjects := true;
  end;

  Assert.AreEqual(ACountAdd, FList.Count);

  for i := 0 to ACountDel - 1 do
  begin
    FList.Remove(FList.First);
  end;
  Assert.AreEqual(ACountAdd - ACountDel, FList.Count);
  if FList.Count > 0 then
    Assert.AreEqual(ACountAdd - 1, FList.Last.Value.ID);
end;

procedure TDLinkedListTest.FindFirst(const ACountAdd, AID, Res: integer);
var
  i: integer;
begin
  FList := TDLinkedList<TTestObj>.Create;
  for i := 0 to ACountAdd - 1 do
  begin
    FList.Add(TTestObj.Create(i));
    FList.Last.OwnsObjects := true;
  end;

  Assert.AreEqual(Res = 1, FList.FindFirst(
    function(const node: TDLinkedList<TTestObj>.TDLinkedNode): boolean
    begin
      Assert.AreEqual(true, node.Value.ID <= AID);
      Result := node.Value.ID = AID;
    end));
end;

procedure TDLinkedListTest.FindFirst_WithRemove(const ACount: integer);
var
  i: integer;
begin
  for i := 0 to ACount - 1 do
  begin
    FList.Add(TTestObj.Create(i));
    FList.Last.OwnsObjects := true;
  end;

  Assert.AreEqual(false, FList.FindFirst(
    function(const node: TDLinkedList<TTestObj>.TDLinkedNode): boolean
    begin
      Assert.AreEqual(true, node.Value.ID <= ACount);
      FList.Remove(node);
      Result := false;
    end));

  Assert.AreEqual(0, FList.Count);
end;

procedure TDLinkedListTest.FindLast(const ACountAdd, AID, Res: integer);
var
  i: integer;
begin
  for i := 0 to ACountAdd - 1 do
  begin
    FList.Add(TTestObj.Create(i));
    FList.Last.OwnsObjects := true;
  end;

  Assert.AreEqual(Res = 1, FList.FindLast(
    function(const node: TDLinkedList<TTestObj>.TDLinkedNode): boolean
    begin
      Assert.AreEqual(true, node.Value.ID >= AID);
      Result := node.Value.ID = AID;
    end));

end;

procedure TDLinkedListTest.FindLast_WithRemove(const ACount: integer);
var
  i: integer;
begin
  for i := 0 to ACount - 1 do
  begin
    FList.Add(TTestObj.Create(i));
    FList.Last.OwnsObjects := true;
  end;

  Assert.AreEqual(false, FList.FindLast(
    function(const node: TDLinkedList<TTestObj>.TDLinkedNode): boolean
    begin
      Assert.AreEqual(true, node.Value.ID >= 0);
      FList.Remove(node);
      Result := false;
    end));

  Assert.AreEqual(0, FList.Count);
end;

procedure TDLinkedListTest.ForEach(const ACount: integer);
var
  i: integer;
  s1, s2: string;
begin
  s1 := '';
  for i := 0 to ACount - 1 do
  begin
    FList.Add(TTestObj.Create(i));
    FList.Last.OwnsObjects := true;
    s1 := s1 + IntToStr(i) + ',';
  end;

  s2 := '';
  FList.ForEach(
    procedure(const node: TDLinkedList<TTestObj>.TDLinkedNode)
    begin
      s2 := s2 + IntToStr(node.Value.ID) + ',';
      Assert.AreEqual(true, node.Value.ID <= ACount);
    end);

  Assert.AreEqual(s1, s2);
end;

procedure TDLinkedListTest.ForEach_WithRemove(const ACount: integer);
var
  i: integer;
  s1, s2: string;
begin
  s1 := '';
  for i := 0 to ACount - 1 do
  begin
    FList.Add(TTestObj.Create(i));
    FList.Last.OwnsObjects := true;
    s1 := IntToStr(i) + ',';
  end;

  s2 := '';
  FList.ForEach(
    procedure(const node: TDLinkedList<TTestObj>.TDLinkedNode)
    begin
      s2 := IntToStr(node.Value.ID) + ',';
      Assert.AreEqual(true, node.Value.ID <= ACount);
      FList.Remove(node);
    end);

  Assert.AreEqual(s1, s2);

  Assert.AreEqual(0, FList.Count);
end;

procedure TDLinkedListTest.Node_OwnsObjectsFalse_ReturnObj;
var
  ANode: TDLinkedList<TTestObj>.TDLinkedNode;
begin
  ANode := TDLinkedList<TTestObj>.TDLinkedNode.Create(TTestObj.Create(0));
  ANode.Free;

  Assert.AreEqual(true, TTestObj.created);
end;

procedure TDLinkedListTest.Node_OwnsObjectsTrue_ReturnNil;
var
  ANode: TDLinkedList<TTestObj>.TDLinkedNode;
begin
  ANode := TDLinkedList<TTestObj>.TDLinkedNode.Create(TTestObj.Create(0));
  ANode.OwnsObjects := true;
  ANode.Free;

  Assert.AreEqual(false, TTestObj.created);
end;

procedure TDLinkedListTest.TearDown;
begin
  FList.Free;
end;

{ TTestObj }

constructor TTestObj.Create(AID: integer);
begin
  created := true;
  ID := AID;
end;

destructor TTestObj.Destroy;
begin
  created := false;
  inherited;
end;

initialization

TDUnitX.RegisterTestFixture(TDLinkedListTest);

end.
