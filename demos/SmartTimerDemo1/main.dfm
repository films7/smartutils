object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'SmartTimer Test: Synchronize event'
  ClientHeight = 431
  ClientWidth = 756
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 608
    Height = 431
    Align = alClient
    ScrollBars = ssBoth
    TabOrder = 0
    ExplicitWidth = 571
  end
  object Panel1: TPanel
    Left = 608
    Top = 0
    Width = 148
    Height = 431
    Align = alRight
    ShowCaption = False
    TabOrder = 1
    ExplicitLeft = 614
    ExplicitTop = 8
    object StartBtn: TButton
      Left = 39
      Top = 38
      Width = 75
      Height = 25
      Caption = 'Start'
      TabOrder = 0
      OnClick = StartBtnClick
    end
    object StopBtn: TButton
      Left = 39
      Top = 69
      Width = 75
      Height = 25
      Caption = 'Stop'
      TabOrder = 1
      OnClick = StopBtnClick
    end
    object SyncCheckBox: TCheckBox
      Left = 40
      Top = 15
      Width = 97
      Height = 17
      Caption = 'Synchronize'
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
  end
end
