unit main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, System.Contnrs;

type
  TMainForm = class(TForm)
    Memo1: TMemo;
    Panel1: TPanel;
    StartBtn: TButton;
    StopBtn: TButton;
    SyncCheckBox: TCheckBox;
    procedure StartBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure StopBtnClick(Sender: TObject);
  private
    { Private-Deklarationen }
    FTimerList: TObjectList;
  public
    { Public-Deklarationen }
    procedure OnTimer(Sender: TObject);
  end;

var
  MainForm: TMainForm;

implementation

uses SmartTimer;

{$R *.dfm}


procedure TMainForm.FormCreate(Sender: TObject);
begin
  FTimerList := TObjectList.Create;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  FTimerList.Free;
end;

procedure TMainForm.OnTimer(Sender: TObject);
var
  s:string;
begin
  s := Format('CurrentThreadID:%.8x Interval:%d %s', [GetCurrentThreadId, TSmartTimer(Sender).Interval, FormatDateTime('hh:nn:ss,zzz', Now)]);
  if TSmartTimer(Sender).EventSynchronized then
      Memo1.Lines.Add(s)
  else
      TThread.Queue(nil,
      procedure
      begin
        Memo1.Lines.Add(s);
      end);
end;

procedure TMainForm.StartBtnClick(Sender: TObject);
var
  i: integer;
  ATimer: TSmartTimer;
begin
  for i := 1 to 10 do
  begin
    ATimer := TSmartTimer.Create(i * 1000);
    FTimerList.Add(ATimer);
    ATimer.EventSynchronized := SyncCheckBox.Checked;
    ATimer.OnTimer := OnTimer;
    ATimer.Enabled := true;
  end;

//  MainThreadID
  Memo1.Lines.Add(Format('MainThreadID:%.8x Start at %s', [GetCurrentThreadId, FormatDateTime('hh:nn:ss,zzz', Now)]));
end;

procedure TMainForm.StopBtnClick(Sender: TObject);
begin
  FTimerList.Clear;
end;

end.
