{-----------------------------------------------------------------------------
Demo of 10000 timers

every 100 milliseconds, timers are created (amount in _TimerProInterval)
the maximum number of intervals in _IntervalCount
timers count =  _TimerProInterval * _IntervalCount

-----------------------------------------------------------------------------}

unit main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, System.Contnrs;

type
  TMainForm = class(TForm)
    Panel1: TPanel;
    StartBtn: TButton;
    StopBtn: TButton;
    Memo1: TMemo;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure StartBtnClick(Sender: TObject);
    procedure StopBtnClick(Sender: TObject);
  private
    { Private-Deklarationen }
    FTimerList: TObjectList;
  public
    { Public-Deklarationen }
    procedure OnTestTimer(Sender: TObject);
  end;

var
  MainForm: TMainForm;

implementation

uses SmartTimer;

{$R *.dfm}


type
  TTestTimer = class(TSmartTimer)
    ID: integer;
  end;

const
  _TimerProInterval = 1000;
  _IntervalCount = 10;

var
  _ViewId: array [0.._IntervalCount - 1] of integer;
  _id: integer = 1;

procedure TMainForm.FormCreate(Sender: TObject);
var
  i: integer;
begin
  FTimerList := nil;

  for i := 0 to _IntervalCount - 1 do
      _ViewId[i] := (i + 1) * _TimerProInterval;
  Caption := Format('SmartTimer Test: %d Timers', [_TimerProInterval * _IntervalCount]);
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  if Assigned(FTimerList) then
      FreeAndNil(FTimerList);
end;

procedure TMainForm.OnTestTimer(Sender: TObject);
var
  AId: integer;
  dt: TDateTime;
  i: integer;
  b: boolean;
begin
  AId := TTestTimer(Sender).ID;

  for i := 0 to High(_ViewId) do
  begin
    b := AId = _ViewId[i];
    if b then
        break;
  end;

  dt := Now;
  TThread.Queue(nil,
    procedure
    begin
      if b then
          Memo1.Lines.Add(Format('TimerId:%d %s', [AId, FormatDateTime('hh:nn:ss,zzz', dt)]));
    end
    );
end;

procedure TMainForm.StartBtnClick(Sender: TObject);
begin
  _id := 1;
  if not Assigned(FTimerList) then
      FTimerList := TObjectList.Create;
  Timer1.Interval := 100;
  Timer1.Enabled := true;
  StartBtn.Enabled := false;
end;

procedure TMainForm.StopBtnClick(Sender: TObject);
begin
  Timer1.Enabled := false;
  if Assigned(FTimerList) then
      FreeAndNil(FTimerList);
  StartBtn.Enabled := true;
end;

procedure TMainForm.Timer1Timer(Sender: TObject);
var
  t: TTestTimer;
  i: integer;
begin
  try
    for i := 1 to _TimerProInterval do
    begin
      t := TTestTimer.Create(3000);

      FTimerList.Add(t);
      t.OnTimer := OnTestTimer;
      t.ID := _id;

      t.Enabled := true;
      _id := _id + 1;

      if _id = (_TimerProInterval*_IntervalCount + 1) then
      begin
        Timer1.Enabled := false;
        Exit;
      end;
    end;
  finally
    Memo1.Lines.Add(Format('create %d %s', [_id - 1, FormatDateTime('hh:nn:ss,zzz', now)]));
  end;
end;

end.
