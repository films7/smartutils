object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'SmartTimer Test: '
  ClientHeight = 411
  ClientWidth = 852
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 704
    Top = 0
    Width = 148
    Height = 411
    Align = alRight
    ShowCaption = False
    TabOrder = 0
    object StartBtn: TButton
      Left = 39
      Top = 38
      Width = 75
      Height = 25
      Caption = 'Start'
      TabOrder = 0
      OnClick = StartBtnClick
    end
    object StopBtn: TButton
      Left = 39
      Top = 69
      Width = 75
      Height = 25
      Caption = 'Stop'
      TabOrder = 1
      OnClick = StopBtnClick
    end
  end
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 704
    Height = 411
    Align = alClient
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 512
    Top = 24
  end
end
