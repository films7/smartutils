{ -----------------------------------------------------------------------------
  Unit Name: Generics.DLinkedList


  MIT License

  Copyright (c) 2020 Oleg Diener

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without
  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the Software
  is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
  OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
  OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  Purpose: Doubly Circular Linked List
  History:

  ----------------------------------------------------------------------------- }

unit Generics.DLinkedList;

interface

uses System.Classes, System.Generics.Collections;

type
  TDLinkedList<T> = class(TEnumerable<T>)
  public type
    TDLinkedNode = class(TObject)
    private
      FPrev, FNext: TDLinkedNode;
      FList: TDLinkedList<T>;
      FValue: T;
      FOwnsObjects: boolean;

      function GetValue: T; inline;
      procedure SetValue(const Val: T); inline;

      function GetNext: TDLinkedNode; inline;
      function GetPrevious: TDLinkedNode; inline;
      function GetList: TDLinkedList<T>; inline;

      procedure SetList(const Val: TDLinkedList<T>); inline;
      procedure SetPrevious(const Val: TDLinkedNode); inline;
      procedure SetNext(const Val: TDLinkedNode); inline;
      procedure Clear;

      function CastToObj(const Value): TObject;
      procedure FreeValue;
    public
      constructor Create; overload;
      constructor Create(const v: T); overload;
      destructor Destroy; override;
      /// <summary>
      /// Deletes the node from the list
      /// </summary>
      procedure DeleteFromList;

      property Value: T read GetValue write SetValue;
      property Next: TDLinkedNode read GetNext;
      property Previous: TDLinkedNode read GetPrevious;
      property List: TDLinkedList<T> read GetList;
      /// <summary>
      /// If OwnsObjects is set to true (default false), Value is released when the destructor is called
      /// </summary>
      /// <returns>None</returns>
      property OwnsObjects: boolean read FOwnsObjects write FOwnsObjects;
    end;

    ForEachFunc = reference to procedure(const node: TDLinkedNode);
    FindFunc = reference to function(const node: TDLinkedNode): boolean;

  private
    FLast: TDLinkedNode;
    FCount: integer;
    function GetFirst: TDLinkedNode;
    function GetLast: TDLinkedNode;
  protected
    procedure ChangeLastNode(const ANewLastNode: TDLinkedNode);
    function DoGetEnumerator: TEnumerator<T>; override;
  public
    constructor Create; virtual;
    destructor Destroy; override;

    /// <summary>
    /// Adds a new node to the end of the list
    /// </summary>
    procedure Add(node: TDLinkedNode); overload; virtual;
    /// <summary>
    /// Creates a new node with a value (v) and adds it to the end of the list
    /// </summary>
    /// <returns>new node</returns>
    function Add(v: T): TDLinkedNode; overload; virtual;

    /// <summary>
    /// Deletes a node from the list
    /// </summary>
    procedure Delete(node: TDLinkedNode); virtual;
    /// <summary>
    /// Removes a node from the list and frees memory for the node
    /// </summary>
    procedure Remove(node: TDLinkedNode); virtual;

    /// <summary>
    /// Releases all nodes from the list
    /// </summary>
    procedure Clear; virtual;
    procedure ForEach(AFunc: ForEachFunc); virtual;
    /// <summary>
    /// Calls the function "AFunc" starting from the head of the list until the end is reached or the desired node is found
    /// </summary>
    /// <param name="AFunc: FindFunc"></param>
    /// <returns>True: if a node is found</returns>
    function FindFirst(AFunc: FindFunc): boolean; virtual;
    /// <summary>
    /// Calls the function "AFunc" starting from the end of the list until the beginning is reached or the desired node is found
    /// </summary>
    /// <param name="AFunc: FindFunc"></param>
    /// <returns>True: if a node is found</returns>
    function FindLast(AFunc: FindFunc): boolean; virtual;

    function Count: integer; virtual;
    function IsEmpty: boolean; virtual;

    property Last: TDLinkedNode read GetLast;
    property First: TDLinkedNode read GetFirst;

  type
    TEnumerator = class(TEnumerator<T>)
    private
      FList: TDLinkedList<T>;
      FIndex: integer;
      FCurrentNode: TDLinkedNode;
      function GetCurrent: T;
    protected
      function DoGetCurrent: T; override;
      function DoMoveNext: boolean; override;
    public
      constructor Create(const AList: TDLinkedList<T>);
      property Current: T read GetCurrent;
      function MoveNext: boolean;
    end;

  function GetEnumerator: TEnumerator; reintroduce; inline;
  end;

implementation

uses
  System.Rtti;

{ TDLinkedList<T> }

procedure TDLinkedList<T>.Add(node: TDLinkedNode);
var
  tmp: TDLinkedNode;
begin
  node.SetList(Self);
  Inc(FCount);

  if not Assigned(FLast) then
  begin
    FLast := node;
    FLast.SetPrevious(FLast);
    FLast.SetNext(FLast);
    Exit;
  end;

  tmp := FLast.Next;

  FLast.SetNext(node);
  node.SetPrevious(FLast);
  node.SetNext(tmp);
  tmp.SetPrevious(node);

  FLast := node;
end;

function TDLinkedList<T>.Add(v: T): TDLinkedNode;
begin
  Result := TDLinkedNode.Create(v);
  Add(Result);
end;

procedure TDLinkedList<T>.ChangeLastNode(const ANewLastNode: TDLinkedNode);
begin
  FLast := ANewLastNode;
end;

procedure TDLinkedList<T>.Clear;
begin
  while Assigned(FLast) do
    Remove(FLast);
end;

function TDLinkedList<T>.Count: integer;
begin
  Result := FCount;
end;

constructor TDLinkedList<T>.Create;
begin
  FLast := nil;
  FCount := 0;
end;

procedure TDLinkedList<T>.Delete(node: TDLinkedNode);
begin
  if not Assigned(FLast) or (node.List <> Self) then
    Exit;

  if node = FLast then
    FLast := node.Previous;

  node.Previous.SetNext(node.Next);
  node.Next.SetPrevious(node.Previous);

  if FLast = node then
    FLast := nil;
  node.Clear;
  Dec(FCount);
end;

procedure TDLinkedList<T>.Remove(node: TDLinkedNode);
begin
  Delete(node);
  node.Free;
end;

destructor TDLinkedList<T>.Destroy;
begin
  Clear;
  inherited;
end;

function TDLinkedList<T>.DoGetEnumerator: TEnumerator<T>;
begin
  Result := GetEnumerator;
end;

function TDLinkedList<T>.FindFirst(AFunc: FindFunc): boolean;
var
  tmp, tmp2: TDLinkedNode;
begin
  if not Assigned(FLast) then
    Exit(false);

  tmp := FLast;
  repeat
    tmp2 := tmp.Next;
    if AFunc(tmp2) then
      Exit(true);
    if tmp2.List = Self then // if tmp2.remove then without next
    begin
      tmp := tmp.Next;
      if (tmp = FLast) then
        break;
    end
    else if (FLast = nil) then
      break;
  until false;

  Result := false;
end;

function TDLinkedList<T>.FindLast(AFunc: FindFunc): boolean;
var
  tmp, tmp2: TDLinkedNode;
begin
  if not Assigned(FLast) then
    Exit(false);

  tmp := First;
  tmp2 := tmp.Previous;
  if AFunc(tmp2) then // Check FLast
    Exit(true);

  if tmp2.List = Self then // if tmp2.remove then without Previous
    tmp := FLast;

  repeat
    tmp2 := tmp.Previous;
    if not Assigned(tmp2) then
      Exit(false);
    if AFunc(tmp2) then
      Exit(true);
    if tmp2.List = Self then // if tmp2.remove then without Previous
    begin
      tmp := tmp.Previous;
      if (tmp = FLast) then
        break;
    end
    else if (FLast = nil) then
      break;
  until false;

  Result := false;
end;

procedure TDLinkedList<T>.ForEach(AFunc: ForEachFunc);
var
  tmp, tmp2: TDLinkedNode;
begin
  if not Assigned(FLast) then
    Exit;

  tmp := FLast;
  repeat
    tmp2 := tmp.Next;
    AFunc(tmp2);
    if tmp2.List = Self then // if tmp2.remove then without next
    begin
      tmp := tmp.Next;
      if (tmp = FLast) then
        break;
    end
    else if (FLast = nil) then
      break;
  until false;
end;

function TDLinkedList<T>.GetEnumerator: TEnumerator;
begin
  Result := TEnumerator.Create(Self);
end;

function TDLinkedList<T>.GetFirst: TDLinkedNode;
begin
  if not Assigned(FLast) then
    Exit(nil);
  Result := FLast.Next;
end;

function TDLinkedList<T>.GetLast: TDLinkedNode;
begin
  Result := FLast;
end;

function TDLinkedList<T>.IsEmpty: boolean;
begin
  Result := not Assigned(FLast);
end;

{ TDLinkedList<T>.TDLinkedNode }

constructor TDLinkedList<T>.TDLinkedNode.Create;
begin
  inherited;
  FPrev := nil;
  FNext := nil;
  FList := nil;
  FOwnsObjects := false;
end;

constructor TDLinkedList<T>.TDLinkedNode.Create(const v: T);
begin
  Create;
  FValue := v;
end;

function TDLinkedList<T>.TDLinkedNode.CastToObj(const Value): TObject;
begin
  Result := TObject(Value);
end;

procedure TDLinkedList<T>.TDLinkedNode.FreeValue;
var
  LType: TRttiType;
  LContext: TRttiContext;
begin
  LContext := TRttiContext.Create;
  try
    LType := LContext.GetType(TypeInfo(T));
    if LType.TypeKind = tkClass then
      CastToObj(FValue).Free;
  finally
    LContext.Free;
  end;
end;

destructor TDLinkedList<T>.TDLinkedNode.Destroy;
begin
  DeleteFromList;

  if FOwnsObjects then
    FreeValue;

  inherited;
end;

function TDLinkedList<T>.TDLinkedNode.GetList: TDLinkedList<T>;
begin
  Result := FList;
end;

function TDLinkedList<T>.TDLinkedNode.GetNext: TDLinkedNode;
begin
  Result := FNext
end;

function TDLinkedList<T>.TDLinkedNode.GetPrevious: TDLinkedNode;
begin
  Result := FPrev
end;

function TDLinkedList<T>.TDLinkedNode.GetValue: T;
begin
  Result := FValue;
end;

procedure TDLinkedList<T>.TDLinkedNode.DeleteFromList;
begin
  if Assigned(FList) then
    FList.Delete(Self);
  Clear;
end;

procedure TDLinkedList<T>.TDLinkedNode.SetList(const Val: TDLinkedList<T>);
begin
  FList := Val
end;

procedure TDLinkedList<T>.TDLinkedNode.SetNext(const Val: TDLinkedNode);
begin
  FNext := Val
end;

procedure TDLinkedList<T>.TDLinkedNode.SetPrevious(const Val: TDLinkedNode);
begin
  FPrev := Val
end;

procedure TDLinkedList<T>.TDLinkedNode.SetValue(const Val: T);
begin
  FValue := Val
end;

procedure TDLinkedList<T>.TDLinkedNode.Clear;
begin
  FList := nil;
  FNext := nil;
  FPrev := nil;
end;

{ TDLinkedList<T>.TEnumerator }

constructor TDLinkedList<T>.TEnumerator.Create(const AList: TDLinkedList<T>);
begin
  inherited Create;
  FList := AList;
  FIndex := -1;
  FCurrentNode := nil;
end;

function TDLinkedList<T>.TEnumerator.DoGetCurrent: T;
begin
  Result := GetCurrent;
end;

function TDLinkedList<T>.TEnumerator.DoMoveNext: boolean;
begin
  Result := MoveNext;
end;

function TDLinkedList<T>.TEnumerator.GetCurrent: T;
begin
  Result := FCurrentNode.Value;
end;

function TDLinkedList<T>.TEnumerator.MoveNext: boolean;
begin
  if FIndex >= FList.Count then
    Exit(false);
  Inc(FIndex);
  Result := FIndex < FList.Count;

  if Result then
    if FIndex = 0 then
      FCurrentNode := FList.First
    else
      FCurrentNode := FCurrentNode.Next;
end;

end.
