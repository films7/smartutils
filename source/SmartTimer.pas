{ -----------------------------------------------------------------------------
  Unit Name: SmartTimer


  MIT License

  Copyright (c) 2020 Oleg Diener

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without
  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the Software
  is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
  OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
  OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  Purpose: A timer that uses as few system resources.
  As far as possible, this is similar to the behavior of the system timer.
  History:

  ----------------------------------------------------------------------------- }

unit SmartTimer;

interface

uses
  System.Classes, System.SyncObjs, System.Generics.Collections,
  Generics.DLinkedList, SafeRef;

type
  TSmartTimer = class;

  TTimerManager = class(TObject)
{$IFDEF TEST}
  public
{$ELSE}
  strict private
{$ENDIF}
  type
    TIntervalManager = class sealed(TDLinkedList<TSmartTimer>)
{$IFDEF TEST}
    public
{$ELSE}
    private
{$ENDIF}
      FRemainingTime: integer; // Remaining time for calculate next timer
      FInterval: integer;
      procedure Add(const ATimer: TSmartTimer); reintroduce;
      procedure Delete(const ATimer: TSmartTimer); reintroduce;
      constructor Create(AInterval: integer); reintroduce;
      procedure Calc(AElapsedTime: Cardinal);
    end;

    // TWorker: check interval
    TWorker = class sealed(TThread)
    private
      FInterval: Cardinal;
      FOwner: TTimerManager;
      FWaitEvent: THandle;
      procedure Stop;
    protected
      procedure Execute; override;
    public
      constructor Create(const AOwner: TTimerManager; AInterval: Cardinal);
      destructor Destroy; override;
    end;
{$IFDEF TEST}
  public
{$ELSE}
  strict private
{$ENDIF}
    FWorker: TWorker;
    FWorkerInterval: Cardinal;
    FIntervalDict: TDictionary<integer, TIntervalManager>;
    // Interval<>TimerList
    FCritSection: TCriticalSection;
    class var FDefault: TTimerManager;

  const
    _WorkerIntervalDef = 50; //in ms
  private
    FWorkerHandle: THandle;
    procedure Add(const ATimer: TSmartTimer);
    procedure Delete(const ATimer: TSmartTimer);
    procedure StopWorker;

    procedure Lock;
    procedure UnLock;
  public
    constructor Create(AWorkerInterval: Cardinal); // check interval
    destructor Destroy; override;

    class procedure Initialize;
    class function Default: TTimerManager;
    class destructor DestroyDefault;
  end;

  TSmartTimer = class(TObject)
  private
    FISafeRef: ISafeRef<TSmartTimer>;

    FEnabled: boolean;
    FOneTime: boolean;
    FInterval: integer;
    FManager: TTimerManager;
    FRemainingInterval: integer;
    FNode: TDLinkedList<TSmartTimer>.TDLinkedNode;
    FTimerEvent: TNotifyEvent;
    FEventSynchronized: boolean;
    FSynchronizeThread: TThread;
    procedure SetEnabled(const Value: boolean);
    procedure DoEvent;
    function GetISafeRef: ISafeRef<TSmartTimer>;
  protected
    procedure TimerEvent; virtual;
  public
    constructor Create(AInterval: integer; AOneTime: boolean = false); overload;
    constructor Create(const AManager: TTimerManager; AInterval: integer;
      AOneTime: boolean = false); overload;
    constructor Create(const AManager: TTimerManager; AInterval: integer;
      const ASynchronizeThread: TThread; AOneTime: boolean = false); overload;
    destructor Destroy; override;
    property Enabled: boolean read FEnabled write SetEnabled;
    property Interval: integer read FInterval;
    property OneTime: boolean read FOneTime;
    property EventSynchronized: boolean read FEventSynchronized
      write FEventSynchronized;

    property OnTimer: TNotifyEvent read FTimerEvent write FTimerEvent;
  end;

implementation

uses
  System.SysUtils, Windows;

{ TSmartTimer }

constructor TSmartTimer.Create(AInterval: integer; AOneTime: boolean);
begin
  Create(TTimerManager.Default, AInterval, TThread.Current, AOneTime);
end;

constructor TSmartTimer.Create(const AManager: TTimerManager;
  AInterval: integer; AOneTime: boolean);
begin
  Create(AManager, AInterval, TThread.Current, AOneTime);
end;

constructor TSmartTimer.Create(const AManager: TTimerManager;
  AInterval: integer; const ASynchronizeThread: TThread; AOneTime: boolean);
begin
  FISafeRef := nil;

  FInterval := AInterval;
  FOneTime := AOneTime;
  FManager := AManager;
  FNode := nil;
  FEventSynchronized := false;
  FSynchronizeThread := ASynchronizeThread;
end;

destructor TSmartTimer.Destroy;
begin
  Enabled := false;
  if Assigned(FISafeRef) then
    FISafeRef.Execute(
      procedure
      begin
        FISafeRef.Cleanup;
      end);
  inherited;
end;

procedure TSmartTimer.TimerEvent;
var
  ARef: ISafeRef<TSmartTimer>;
begin
  if Assigned(FTimerEvent) then
    if FEventSynchronized then
    begin
      ARef := GetISafeRef; // save the "self" object
      TThread.Queue(FSynchronizeThread,
      // if FSynchronizeThread=nil then in main thread
        procedure // another thread
        begin
          if ARef.Lock then // check the "self" object alive
            try
              FTimerEvent(Self);
            finally
              ARef.UnLock;
            end;
        end);
    end
    else
      FTimerEvent(Self);
end;

procedure TSmartTimer.DoEvent;
begin
  if FOneTime then
    FEnabled := false;
  TimerEvent;
end;

function TSmartTimer.GetISafeRef: ISafeRef<TSmartTimer>;
begin
  if not Assigned(FISafeRef) then
    FISafeRef := TSafeRef<TSmartTimer>.Create(Self) as ISafeRef<TSmartTimer>;
  Result := FISafeRef;
end;

procedure TSmartTimer.SetEnabled(const Value: boolean);
begin
  if FEnabled <> Value then
  begin
    FEnabled := Value;
    if Value then
      FManager.Add(Self)
    else
      FManager.Delete(Self);
  end;
end;

{ TTimerManager }

constructor TTimerManager.Create(AWorkerInterval: Cardinal);
begin
  FIntervalDict := TObjectDictionary<integer, TIntervalManager>.Create
    ([doOwnsValues]);
  FWorker := nil;
  FWorkerInterval := AWorkerInterval;
  FCritSection := TCriticalSection.Create;
  FWorkerHandle := INVALID_HANDLE_VALUE;
end;

class procedure TTimerManager.Initialize;
begin
  Default;
end;

class function TTimerManager.Default: TTimerManager;
begin
  if not Assigned(FDefault) then
    FDefault := TTimerManager.Create(_WorkerIntervalDef);
  Result := FDefault;
end;

class destructor TTimerManager.DestroyDefault;
begin
  if Assigned(FDefault) then
    FreeAndNil(FDefault);
end;

procedure TTimerManager.Add(const ATimer: TSmartTimer);
var
  AIvlMnr: TIntervalManager;
begin
  Lock;
  try
    if not FIntervalDict.TryGetValue(ATimer.Interval, AIvlMnr) then
    begin
      AIvlMnr := TIntervalManager.Create(ATimer.Interval);
      FIntervalDict.Add(AIvlMnr.FInterval, AIvlMnr);
    end;
    AIvlMnr.Add(ATimer);
{$IFNDEF TEST}
    if not Assigned(FWorker) then
      FWorker := TWorker.Create(Self, FWorkerInterval);
{$ENDIF}
  finally
    UnLock;
  end;
end;

procedure TTimerManager.Delete(const ATimer: TSmartTimer);
var
  AIvlMnr: TIntervalManager;
begin
  Lock;
  try
    if FIntervalDict.TryGetValue(ATimer.Interval, AIvlMnr) then
    begin
      AIvlMnr.Delete(ATimer);
      if AIvlMnr.IsEmpty then
        FIntervalDict.Remove(ATimer.Interval);
    end;

    if FIntervalDict.Count = 0 then
      StopWorker;
  finally
    UnLock;
  end;
end;

procedure TTimerManager.StopWorker;
begin
  if Assigned(FWorker) then
  begin
    FWorker.Stop;
    FWorker := nil;
  end;
end;

destructor TTimerManager.Destroy;
begin
  Lock;
  try
    FreeAndNil(FIntervalDict);
    StopWorker;
  finally
    UnLock;
  end;

  if INVALID_HANDLE_VALUE <> FWorkerHandle then
    Windows.WaitForSingleObject(FWorkerHandle, INFINITE);
  // wait for the end of the worker

  if FDefault = Self then
    FDefault := nil;
  FCritSection.Free;
end;

procedure TTimerManager.Lock;
begin
  FCritSection.Enter;
end;

procedure TTimerManager.UnLock;
begin
  FCritSection.Leave;
end;

{ TTimerManager.TWorker }

constructor TTimerManager.TWorker.Create(const AOwner: TTimerManager;
AInterval: Cardinal);
begin
  FInterval := AInterval;
  FreeOnTerminate := true;
  FOwner := AOwner;

  FWaitEvent := Windows.CreateEvent(nil, true, false, nil);
  inherited Create;
end;

destructor TTimerManager.TWorker.Destroy;
begin
  Windows.CloseHandle(FWaitEvent);
  inherited;
end;

procedure TTimerManager.TWorker.Execute;
var
  AKeys: TArray<integer>;
  i: integer;
  AMng: TIntervalManager;
begin
  try
    while not Terminated do
    begin
      if WaitForSingleObject(FWaitEvent, FInterval) = WAIT_OBJECT_0 then
        // if the signal is given stop else wait a interval
        Exit;

      FOwner.Lock;
      try
        if Terminated then
          Exit;
        if not Assigned(FOwner.FIntervalDict) then
        begin
          FOwner.StopWorker;
          Exit;
        end;

        AKeys := FOwner.FIntervalDict.Keys.ToArray;
        for i := 0 to Length(AKeys) - 1 do
        begin
          if FOwner.FIntervalDict.TryGetValue(AKeys[i], AMng) then
          begin
            AMng.Calc(FInterval);
            if AMng.Count = 0 then
              FOwner.FIntervalDict.Remove(AKeys[i]);
          end;
        end;

        if FOwner.FIntervalDict.Count = 0 then
        begin
          FOwner.StopWorker;
          Exit;
        end;

      finally
        FOwner.UnLock;
      end;
    end;
  finally
    FOwner.FWorkerHandle := INVALID_HANDLE_VALUE;
  end;
end;

procedure TTimerManager.TWorker.Stop;
begin
  FOwner.FWorkerHandle := Handle;
  Windows.SetEvent(FWaitEvent);
  Terminate;
end;

{ TTimerManager.TIntervalManager }

procedure TTimerManager.TIntervalManager.Add(const ATimer: TSmartTimer);
begin
  ATimer.FRemainingInterval := FInterval - FRemainingTime;
  FRemainingTime := FInterval;

  ATimer.FNode := inherited Add(ATimer);
end;

procedure TTimerManager.TIntervalManager.Calc(AElapsedTime: Cardinal);
var
  ALastFireTimer: TDLinkedNode;
  ARemainingInterval: integer;
begin
  if Count = 0 then
    Exit;

  Dec(First.Value.FRemainingInterval, AElapsedTime);
  Dec(FRemainingTime, AElapsedTime);

  ALastFireTimer := nil;
  ARemainingInterval := 0;

  FindFirst(
    function(const v: TDLinkedNode): boolean
    var
      p: Pointer;
    begin
      if v.Value.FRemainingInterval > 0 then
      begin
        ARemainingInterval := v.Value.FRemainingInterval;
        Exit(true);
      end;

      p := @v.Value.FNode;
      v.Value.DoEvent;
      if Assigned(Pointer(p^)) and (v.Value.FNode.List = Self) then
        // if SmartTimer stopped then p^=nil
        if (not v.Value.Enabled) then
        begin
          Pointer(p^) := nil; // = TSmartTimer.FNode := nil
          v.Free;
        end
        else
        begin
          ALastFireTimer := v;
          v.Value.FRemainingInterval := FInterval - FRemainingTime;
          FRemainingTime := FInterval;
        end;
      Result := false;
    end);

  if FRemainingTime <= 0 then
    FRemainingTime := FInterval - ARemainingInterval;
  if Assigned(ALastFireTimer) then
    ChangeLastNode(ALastFireTimer);
end;

constructor TTimerManager.TIntervalManager.Create(AInterval: integer);
begin
  inherited Create();
  FInterval := AInterval;
  FRemainingTime := 0;
end;

procedure TTimerManager.TIntervalManager.Delete(const ATimer: TSmartTimer);
var
  AInterval: integer;
begin
  if Assigned(ATimer.FNode) then
  begin
    if Last <> ATimer.FNode then
      if Assigned(ATimer.FNode.Next) then
      begin
        AInterval := ATimer.FNode.Value.FRemainingInterval;
        if AInterval < 0 then
          AInterval := 0;
        Inc(ATimer.FNode.Next.Value.FRemainingInterval, AInterval);
      end;
    ATimer.FNode.Free; // DeleteFromList;
    ATimer.FNode := nil;
  end;
end;

end.
