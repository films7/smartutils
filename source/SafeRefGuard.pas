{-----------------------------------------------------------------------------
 Unit Name: SafeRefGuard


 MIT License

 Copyright (c) 2020 Oleg Diener

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without
 limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the Software
 is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
 OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 Purpose: Helper record for ISafeRef
 History:

 usage example:
  interface
  type
      TMonitoringObj = class
        private
          G: SafeRefG<TMonitoringObj>;
        public
          constructor Create;
          function GetISafeRef: ISafeRef<TMonitoringObj>;
      end;

  implementation
    constructor TMonitoringObj.Create;
    begin
      G.C(TSafeRef<TMonitoringObj>.Create(Self) as ISafeRef<TMonitoringObj>);
    end;

    function TMonitoringObj.GetISafeRef: ISafeRef<TMonitoringObj>;
    begin
      Result := G;
    end;

-----------------------------------------------------------------------------}

unit SafeRefGuard;

interface

uses SafeRef;

type
  IGuard = interface
    function GetSelfObject: TObject;
  end;

  TSafeRefGuard<T> = class sealed(TInterfacedObject, IGuard)
  private
    FObj: ISafeRef<T>;
    procedure SetT(AVal: ISafeRef<T>);
    procedure Cleanup;
  public
    constructor Create(AVal: ISafeRef<T>); overload;
    destructor Destroy; override;

    function Get: ISafeRef<T>; inline;
    function GetSelfObject: TObject;
  end;

  SafeRefG<T> = record
  private
    FGuard: IGuard;
  public
    constructor C(AVal: ISafeRef<T>);

    function Get(): ISafeRef<T>; inline;
    procedure Reset;
    function Lock: Boolean;
    procedure UnLock;
    class operator Implicit(a: SafeRefG<T>): ISafeRef<T>;
    class operator Explicit(a: SafeRefG<T>): ISafeRef<T>;
  end;

implementation

{ TSafeRefGuard<T> }

constructor TSafeRefGuard<T>.Create(AVal: ISafeRef<T>);
begin
  inherited Create;
  SetT(AVal);
{$IFDEF DEBUG_STRING}
  SendDebugString('Create', Self);
{$ENDIF}
end;

procedure TSafeRefGuard<T>.Cleanup;
var
  ARef: ISafeRef<T>;
begin
  if not Assigned(FObj) then
    Exit;
  ARef := FObj;
  ARef.Execute(
    procedure
    begin
      ARef.Cleanup;
    end);
end;

destructor TSafeRefGuard<T>.Destroy;
begin
  Cleanup;
{$IFDEF DEBUG_STRING}
  SendDebugString('Destroy', Self);
{$ENDIF}
  inherited;
end;

function TSafeRefGuard<T>.Get: ISafeRef<T>;
begin
  Result := FObj;
end;

function TSafeRefGuard<T>.GetSelfObject: TObject;
begin
  Result := Self;
end;

procedure TSafeRefGuard<T>.SetT(AVal: ISafeRef<T>);
begin
  FObj := AVal;
end;

{ SafeRefG<T> }

constructor SafeRefG<T>.C(AVal: ISafeRef<T>);
begin
  FGuard := TSafeRefGuard<T>.Create(AVal) as IGuard;
end;

class operator SafeRefG<T>.Explicit(a: SafeRefG<T>): ISafeRef<T>;
begin
  Result := a.Get;
end;

function SafeRefG<T>.Get: ISafeRef<T>;
begin
  Result := TSafeRefGuard<T>(FGuard.GetSelfObject).Get;
end;

class operator SafeRefG<T>.Implicit(a: SafeRefG<T>): ISafeRef<T>;
begin
  Result := a.Get;
end;

function SafeRefG<T>.Lock: Boolean;
begin
  Result := Get.Lock;
end;

procedure SafeRefG<T>.Reset;
begin
  FGuard := nil;
end;

procedure SafeRefG<T>.UnLock;
begin
  Get.UnLock;
end;

end.
