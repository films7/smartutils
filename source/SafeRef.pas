{-----------------------------------------------------------------------------
 Unit Name: SafeRef


 MIT License

 Copyright (c) 2020 Oleg Diener

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without
 limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the Software
 is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
 OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 Purpose:   Refernce to an object with the ability to check the object alive
 History:

-----------------------------------------------------------------------------}

unit SafeRef;

interface

uses
  Classes, SysUtils;

{$IFDEF DEBUG}
//{$DEFINE DEBUG_STRING}
{$ENDIF}

type
  ISafeRef<T> = interface
/// <summary>
/// Deactivate the reference to the owner object
/// </summary>
    procedure Cleanup;
/// <summary>
/// Check that the owner object is alive
/// </summary>
/// <returns>
/// True: If the owner object is alive, else False
/// </returns>
    function IsAlive: Boolean;
    function Get: T;
    function Lock: Boolean;
    procedure UnLock;
/// <summary>
/// Thread safe version of IsAlive function.
/// Check whether the owner object is active and if so, then the procedure AProc is executed
/// </summary>
/// <param name="AProc: TProc">
/// The execution procedure of type <see cref="System.SysUtils.TProc"> TProc </see>
/// </param>
/// <returns>
/// True: If the owner object is alive, else False
/// </returns>
/// <remarks>
/// A parameter "AProc" must be not nil
/// </remarks>
    function Execute(AProc: TProc): Boolean; overload;
/// <summary>
/// Overload version of <see cref="Execute"> Execute</see> function
/// </summary>
/// <param name="AProc: TProc&lt;T&gt;">
/// The execution procedure of type <see cref="System.SysUtils.TProc"> TProc&lt;T&gt; </see>
/// </param>
    function Execute(AProc: TProc<T>): Boolean; overload;

    function GetObjStatus: integer;
    procedure SetObjStatus(AVal: integer);

/// <summary>
/// Stores an integer value. ObjStatus has no predefined meaning.
/// </summary>
    property ObjStatus: integer read GetObjStatus write SetObjStatus;
  end;

  TSafeRef<T: class> = class(TInterfacedObject, ISafeRef<T>)
  private
    FOwner: T;
    FAlive: Boolean;
    FObjStatus: integer;
  public
    procedure Cleanup;
    function IsAlive: Boolean;
    function Get: T;
    function Lock: Boolean;
    procedure UnLock;
    function GetObjStatus: integer;
    procedure SetObjStatus(AVal: integer);
    function Execute(AProc: TProc): Boolean; overload;
    function Execute(AProc: TProc<T>): Boolean; overload;
    constructor Create(const AOwner: T);
{$IFDEF DEBUG_STRING}
    destructor Destroy; override;
{$ENDIF}
  end;

{$IFDEF DEBUG_STRING}
procedure SendDebugString(const AFuncName: string; const AObj: TObject); inline;
{$ENDIF}

implementation

{$IFDEF DEBUG_STRING}

uses
  Windows;

procedure SendDebugString(const AFuncName: string; const AObj: TObject); inline;
begin
{$WARN UNSAFE_CAST OFF}
  OutputDebugString(PChar(Format('%s.%s (%d)', [AObj.ClassName, AFuncName, NativeInt(AObj)])));
{$WARN UNSAFE_CAST DEFAULT}
end;

{$ENDIF}

{ TSafeRef<T> }

procedure TSafeRef<T>.Cleanup;
begin
  FAlive := false;
  FOwner := nil;
end;

constructor TSafeRef<T>.Create(const AOwner: T);
begin
  FAlive := true;
  FOwner := AOwner;
  FObjStatus := 0;
{$IFDEF DEBUG_STRING}
  SendDebugString('Create', Self);
{$ENDIF}
end;

{$IFDEF DEBUG_STRING}

destructor TSafeRef<T>.Destroy;
begin
  SendDebugString('Destroy', Self);
  inherited;
end;
{$ENDIF}


function TSafeRef<T>.Get: T;
begin
  Result := FOwner;
end;

function TSafeRef<T>.GetObjStatus: integer;
begin
  Result := FObjStatus
end;

function TSafeRef<T>.IsAlive: Boolean;
begin
  Result := FAlive;
end;

function TSafeRef<T>.Lock: Boolean;
begin
  TMonitor.Enter(Self);
  Result := IsAlive;
  if not Result then
      UnLock;
end;

function TSafeRef<T>.Execute(AProc: TProc): Boolean;
begin
  Result := Lock;
  if Result then
    try
      AProc();
    finally
      UnLock;
    end;
end;

function TSafeRef<T>.Execute(AProc: TProc<T>): Boolean;
begin
  Result := Lock;
  if Result then
    try
      AProc(FOwner);
    finally
      UnLock;
    end;
end;

procedure TSafeRef<T>.SetObjStatus(AVal: integer);
begin
  FObjStatus := AVal
end;

procedure TSafeRef<T>.UnLock;
begin
  TMonitor.Exit(Self);
end;

end.
