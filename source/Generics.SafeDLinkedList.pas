{-----------------------------------------------------------------------------
 Unit Name: Generics.SafeDLinkedList


 MIT License

 Copyright (c) 2020 Oleg Diener

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without
 limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the Software
 is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
 OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 Purpose:  Thread-safe version of TDLinkedList
 History:

-----------------------------------------------------------------------------}

unit Generics.SafeDLinkedList;

interface

uses System.Classes, Generics.DLinkedList;

type
  TSafeDLinkedList<T> = class(TDLinkedList<T>)
  public
    procedure Add(v: TDLinkedList<T>.TDLinkedNode); overload; override;
    procedure Delete(v: TDLinkedList<T>.TDLinkedNode); override;
    procedure Clear; override;
    procedure ForEach(AFunc: TDLinkedList<T>.ForEachFunc); override;
    function FindFirst(AFunc: TDLinkedList<T>.FindFunc): boolean; override;

    function Count: integer; override;
    function IsEmpty: boolean; override;
  end;

implementation

{ TSafeDLinkedList<T> }

procedure TSafeDLinkedList<T>.Add(v: TDLinkedList<T>.TDLinkedNode);
begin
  TMonitor.Enter(Self);
  try
    inherited Add(v);
  finally
    TMonitor.Exit(Self);
  end;
end;

procedure TSafeDLinkedList<T>.Clear;
begin
  TMonitor.Enter(Self);
  try
    inherited;
  finally
    TMonitor.Exit(Self);
  end;
end;

function TSafeDLinkedList<T>.Count: integer;
begin
  TMonitor.Enter(Self);
  try
    Result := inherited Count;
  finally
    TMonitor.Exit(Self);
  end;
end;

procedure TSafeDLinkedList<T>.Delete(v: TDLinkedList<T>.TDLinkedNode);
begin
  TMonitor.Enter(Self);
  try
    inherited Delete(v);
  finally
    TMonitor.Exit(Self);
  end;
end;

function TSafeDLinkedList<T>.FindFirst(AFunc: TDLinkedList<T>.FindFunc): boolean;
begin
  TMonitor.Enter(Self);
  try
    Result := inherited FindFirst(AFunc);
  finally
    TMonitor.Exit(Self);
  end;
end;

procedure TSafeDLinkedList<T>.ForEach(AFunc: TDLinkedList<T>.ForEachFunc);
begin
  TMonitor.Enter(Self);
  try
    inherited ForEach(AFunc);
  finally
    TMonitor.Exit(Self);
  end;
end;

function TSafeDLinkedList<T>.IsEmpty: boolean;
begin
  TMonitor.Enter(Self);
  try
    Result := inherited IsEmpty;
  finally
    TMonitor.Exit(Self);
  end;
end;

end.
